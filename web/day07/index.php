<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <!-- Lấy ra biến global -->
    <?php 
        include ("./variable.php");
    ?>

    <div class="center">
        <div class="wrapper search">
            <div class="flex justify-center">
                <div>
                    <div class="label-row department">
                        <span class="compulsory">Khoa</span>
                    </div>
                    <div class="label-row department">
                        <span>Từ khóa</span>
                    </div>
                </div>
                <div>
                    <div class="label-row department">
                        <div class="select-box">
                            <input id="select-input" readonly type="text" name="department">
                            <div class="arrow-down" id="button-dropdown"></div>
                            <ul class="dropdown hide" tabindex="-1">
                                <?php
                                    $departments = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                                    foreach ($departments as $department => $department_value) {
                                        echo "<li value=$department>$department_value</li>";                        
                                    }
                                ?>  
                            </ul>
                        </div>
                    </div>
                    <div class="label-row keyword">
                        <input type="text" name="keyword">
                    </div>
                    <div class="submit">
                        <input type="submit" value="Tìm kiếm" name="submit">
                    </div>
                </div>
            </div>
            <div class="flex justify-between items-center mt-40">
                <div class="number-student">
                    <span>Số sinh viên tìm thấy:</span>
                    <span><?php echo count($students) ?></span>
                </div>
                <div class="add-button">
                    <form action="pages/register.php">
                        <input type="submit" class="button-md" value="Thêm">
                    </form>
                </div>
            </div>
            <div class="table-student mt-40">
                <table>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tên sinh viên</th>
                            <th>Khoa</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $order = 0;
                            foreach ($students as $name => $fac) {
                                $order++;
                                echo '<tr>
                                    <td>'.$order.'</td>
                                    <td>'.$name.'</td>
                                    <td>'.$fac.'</td>
                                    <td class="action-table">
                                        <input type="submit" class="button-sm" value="Xóa">
                                        <input type="submit" class="button-sm ml-10" value="Sửa">
                                    </td>
                                </tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
<style>
<?php 
    include './styles/global.css';
    include './styles/search.css';
?>
</style>

<script src="./js/dropdown.js"></script>
</html>